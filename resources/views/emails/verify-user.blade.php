<section class="invoice">
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header">
                    <a href="http://localhost/cgo1/public/home">
                        <img class="logo" src="http://localhost/cgo1/public/assets/img/logo.png" alt="logo">
                    </a>
                <small class="pull-right"> {{ date('l, F j Y') }} </small>
            </h2>
        </div>
        <!-- /.col -->
    </div>

    <div class="row invoice-info">
        <div class="col-sm-6 invoice-col">
            From
            <address>
                <strong>CGO1</strong><br>
                Office : Apt.123, Venu-123<br>
                Lahore, Punjab, 54000, Pakistan<br>
                Phone: +92-123-123-4567<br>
                Email: cargoservicespk@gmail.​com
            </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-6 invoice-col">
            To
            <address>
                <strong> New User </strong><br>
                Email: new-member@gmail.com
            </address>
        </div>
    </div>
<div class="row">
    <div class="col-xs-12">
        <h1>Click the Link To Verify Your Email</h1>
        <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            Click the following link to verify your email {{ route('verify.email', $email_token) }}
        </p>
        Regards,<br />
        CGO1
    </div>
</div>

</section>