<!DOCTYPE html>
<html>

@section('auth-head-section')
    @include('layouts.admin.auth.head')
@show

@section('bodyStart')
<body>
@show

<!-- CONTENT SECTION -->
@section('auth-content-section')
    @include('layouts.admin.auth.content')
@show
<!-- CONTENT SECTION -->

<!-- Libraries -->
@section('auth-libraries')
<!-- jQuery 3 -->
<script src="{{ asset('assets/admin/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('assets/admin/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
@show
<!-- Libraries -->

<!-- Main Scripts -->
@section('auth-mainscripts')
    <!-- iCheck -->
    <script src="{{ asset('assets/admin/plugins/iCheck/icheck.min.js') }}"></script>
    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' /* optional */
            });
        });
    </script>
@show
<!-- Main Scripts -->

<!-- Excluded Section -->
@section('auth-lockscreen')
@endsection
</body>
</html>
