<!-- Login -->
@section('auth-login')
    <div class="login-box">
        <div class="login-logo">
            <a href="{{ url('home') }}"><img class="logo" src="{{ asset('assets/img/logo.png') }}" alt="logo"></a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">{{ __('Sign in to start your session') }}</p>

            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group has-feedback">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="{{ __('E-Mail Address') }}" required autofocus>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    @if ($errors->has('email'))
                        <span class="invalid-credentials">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback">
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" name="password" required>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    @if ($errors->has('password'))
                        <span class="invalid-credentials">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                            </label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Sign In') }}
                        </button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
<!--
            <div class="social-auth-links text-center">
                <p>- OR -</p>
                <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
                    Facebook</a>
                <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
                    Google+</a>
            </div>
            <!-- /.social-auth-links -->

            <a href="{{ route('password.request') }}">
                {{ __('Forgot Your Password?') }}
            </a>
            <br />
            <a class="text-center" href="{{ route('register') }}">{{ __('Register a new membership') }}</a>

        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- Login Section -->
@show

<!-- Registration -->
@section('auth-register')
    <div class="register-box">
        <div class="register-logo">
            <a href="{{ url('home') }}"><img class="logo" src="{{ asset('assets/img/logo.png') }}" alt="logo"></a>
        </div>

        <div class="register-box-body">
            <p class="login-box-msg">{{ __('Register a new membership') }}</p>

            <form method="POST" action="{{ route('register') }}">
                @csrf
                <div class="form-group has-feedback">
                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Full Name" name="name" value="{{ old('name') }}" required autofocus>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    @if ($errors->has('name'))
                        <span class="invalid-credentials">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email" name="email" value="{{ old('email') }}" required>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    @if ($errors->has('email'))
                        <span class="invalid-credentials">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback">
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" name="password" required>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    @if ($errors->has('password'))
                        <span class="invalid-credentials">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback">
                    <input id="password-confirm" type="password" class="form-control" placeholder="Retype Password" name="password_confirmation" required>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <!--
                    <div class="col-xs-8">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox"> I agree to the <a href="#">terms</a>
                            </label>
                        </div>
                    </div>
                    -->
                    <!-- /.col -->
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Register') }}
                        </button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
<!--
            <div class="social-auth-links text-center">
                <p>- OR -</p>
                <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using
                    Facebook</a>
                <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign up using
                    Google+</a>
            </div>
-->         <br />
            <a href="{{ route('login') }}" class="text-center">I already have a membership</a>
        </div>
        <!-- /.form-box -->
    </div>
    <!-- Registration Section -->
@show

<!-- Reset Email -->
@section('auth-reset-email')
    <div class="login-box">
        <div class="login-logo">
            <a href="{{ url('home') }}"><img class="logo" src="{{ asset('assets/img/logo.png') }}" alt="logo"></a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">{{ __('Password Reset') }}</p>

            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <form method="POST" action="{{ route('password.email') }}">
                @csrf
                <div class="form-group has-feedback">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="E-mail Address" name="email" value="{{ old('email') }}" required>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    @if ($errors->has('email'))
                        <span class="invalid-credentials">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="row">
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Send Password Reset Link') }}
                        </button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
            <br />
            <a href="{{ route('password.request') }}">
                {{ __('Forgot Your Password?') }}
            </a>
            <br />
            <a class="text-center" href="{{ route('register') }}">{{ __('Register a new membership') }}</a>

        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- Reset Password Section -->
@show
<!-- Reset -->

@section('auth-reset')
    <div class="login-box">
        <div class="login-logo">
            <a href="{{ url('home') }}"><img class="logo" src="{{ asset('assets/img/logo.png') }}" alt="logo"></a>
        </div>
        <!-- logo -->
        <div class="login-box-body">
            <p class="login-box-msg">{{ __('Reset Password') }}</p>
            <form method="POST" action="{{ route('password.request') }}">
                @csrf
                <input type="hidden" name="token" value="@yield('token')">
                <div class="form-group has-feedback">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="E-mail Address" name="email" value="{{ $email ?? old('email') }}" required autofocus>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    @if ($errors->has('email'))
                        <span class="invalid-credentials">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback">
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" name="password" required>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    @if ($errors->has('password'))
                        <span class="invalid-credentials">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback">
                    <input id="password-confirm" type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" placeholder="Confirm Password" name="password_confirmation" required>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    @if ($errors->has('password_confirmation'))
                        <span class="invalid-credentials">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="row">
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Reset Password') }}
                        </button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
    </div>
    <!-- Reset Password Section -->
@show

@section('auth-lockscreen')
<!-- Lock Screen -->
<!-- Automatic element centering -->
<div class="lockscreen-wrapper">
    <div class="lockscreen-logo">
        <a href="{{ url('home') }}"><img class="logo" src="{{ asset('assets/img/logo.png') }}" alt="CGO1"></a>
    </div>
    <!-- User name -->
    <div class="lockscreen-name">@yield('username')</div>

    <!-- START LOCK SCREEN ITEM -->
    <div class="lockscreen-item">
        <!-- lockscreen image -->
        <div class="lockscreen-image">
            <img src="@yield('user-profile-pic')" alt="User Image">
        </div>
        <!-- /.lockscreen-image -->

        <!-- lockscreen credentials (contains the form) -->
        <form class="lockscreen-credentials" method="POST" action="{{ route('lockscreen') }}">
            @csrf
            <div class="input-group">
                <input type="password" name="password" class="form-control" placeholder="Password" />
                <div class="input-group-btn">
                    <button type="submit" class="btn"><i class="fa fa-arrow-right text-muted"></i></button>
                </div>
            </div>
        </form>
        <!-- /.lockscreen credentials -->

    </div>
    <!-- /.lockscreen-item -->
    <div class="help-block text-center">
        Enter your password to retrieve your session
    </div>
    <div class="text-center">
        <a href="@yield('loginurl')">Or sign in as a different user</a>
    </div>
    <div class="lockscreen-footer text-center">
        Copyright &copy; 2017-2018 <a href="javascript: void(0)" class="text-black"> <strong>Creative Developers</strong> </a> <br>
        All rights reserved
    </div>
</div>
<!-- /.center -->
@show

@section('email-confirm')
    <div class="login-box">
        <div class="login-logo">
            <a href="{{ url('home') }}"><img class="logo" src="{{ asset('assets/img/logo.png') }}" alt="logo"></a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body text-center">
            <h3 class="alert-success" style="color: #1c7430;">{{ __('Registration Confirmed') }}</h3>
            <p class="login-box-msg">
                Your Email is successfully verified.
                Click here to <a href="{{ url('admin/login') }}">login</a>
            </p>
        </div>
    </div>
@show

@section('email-verification')
    <div class="login-box">
        <div class="login-logo">
            <a href="{{ url('home') }}"><img class="logo" src="{{ asset('assets/img/logo.png') }}" alt="logo"></a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body text-center">
            <h3 class="alert-success">{{ __('Registration Verification Pending...') }}</h3>
            <p class="login-box-msg">
                You have successfully registered.
                An email is sent to you for verification.
            </p>
        </div>
    </div>
@show