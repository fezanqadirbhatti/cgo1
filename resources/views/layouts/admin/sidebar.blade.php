@section('admin-sidebar')
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar user panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ url('storage/avatars') .'/'. Auth::user()->avatar }}" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <a href="javascript:void(0)"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>

            <!-- search form (Optional) -->
            <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
                </div>
            </form>
            <!-- /.search form -->
        @if(Auth::user()->hasRole('admin'))
            <!-- Full Sidebar Menu -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">Main Navigation</li>
                <!-- Optionally, you can add icons to the links -->
                <li {{{ (Request::is('admin/home') ? 'class=active' : '') }}}>
                    <a href="{{ route('home') }}">
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li {{{ (Request::is('admin/tracking') ? 'class=active' : '') }}}>
                    <a href="{{ route('tracking') }}">
                        <i class="fa fa-truck"></i>
                        <span>Consignment Tracking</span>
                    </a>
                </li>
                <li {{{ (Request::is('admin/invoice') ? 'class=active' : '') }}}>
                    <a href="{{ route('invoice') }}">
                        <i class="fa fa-list"></i>
                        <span>Invoices</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="javascript:void(0)"><i class="fa fa-wrench"></i> <span>Manage</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="javascript:void(0)"><i class="fa fa-address-book"></i> Address Book </a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-location-arrow"></i> Zone </a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-map-signs"></i> Routes </a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-fw fa-calendar-check-o"></i> Bookings </a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-info-circle"></i> Inquires </a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-users"></i> Users </a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-envelope-open"></i> News Letter </a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <i class="fa fa-fw fa-calendar-plus-o"></i>
                        <span>Booking Details</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <i class="fa fa-fw fa-money"></i>
                        <span>Pricing</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <i class="fa fa-check-square"></i>
                        <span>Payment Terms</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <i class="fa fa-fw fa-buysellads"></i>
                        <span>Compaign</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <i class="fa fa-fw fa-question-circle"></i>
                        <span>FAQs</span>
                    </a>
                </li>
            </ul>
            <!-- /.sidebar-menu -->
        @elseif(Auth::user()->hasRole('user'))
            <!-- Limited Access Sidebar Menu -->
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header">Main Navigation</li>
                    <!-- Optionally, you can add icons to the links -->
                    <li {{{ (Request::is('admin/user/dashboard') ? 'class=active' : '') }}}>
                        <a href="{{ route('userDashboard') }}">
                            <i class="fa fa-home"></i>
                            <span>User Home</span>
                        </a>
                    </li>
                    <li {{{ (Request::is('admin/tracking') ? 'class=active' : '') }}}>
                        <a href="{{ route('tracking') }}">
                            <i class="fa fa-truck"></i>
                            <span>Consignment Tracking</span>
                        </a>
                    </li>
                    <li {{{ (Request::is('admin/invoice') ? 'class=active' : '') }}}>
                        <a href="{{ route('invoice') }}">
                            <i class="fa fa-list"></i>
                            <span>Invoices</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <i class="fa fa-fw fa-calendar-plus-o"></i>
                            <span>Booking Details</span>
                        </a>
                    </li>
                </ul>
                <!-- /.sidebar-menu -->
        @endif
        </section>
        <!-- /.sidebar -->
    </aside>
@show