@section('admin-footer')
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2014-{{ date('Y') }}
        <a href="{{ url('') }}">
            <span id="admin-copyright">CGO1</span>
        </a>.
    </strong>
    All rights reserved.
</footer>
@show