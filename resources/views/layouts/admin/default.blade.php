<!DOCTYPE html>
<html>

<!-- HEAD -->
@section('admin-head')
    @include('layouts.admin.head')
@show
<!-- HEAD -->

@section('bodyStart')
<body class="hold-transition skin-blue sidebar-mini">
@show
    <div class="wrapper">

        <!-- HEADER -->
        @section('admin-header')
            @include('layouts.admin.header')
        @show
        <!-- HEADER -->

        <!-- Left side column. contains the logo and sidebar -->
        @section('admin-sidebar')
            @include('layouts.admin.sidebar')
        @show
        <!-- Left side column. contains the logo and sidebar -->

        <!-- Content Wrapper. Contains page content -->
        @section('admin-page-content')
            @include('layouts.admin.content')
        @show
        <!-- /.content-wrapper -->

        <!-- FOOTER -->
        @section('admin-footer')
            @include('layouts.admin.footer')
        @show
        <!-- FOOTER -->

        <!-- Control Sidebar -->
        @section('admin-control-sidebar')
            @include('layouts.admin.control-sidebar')
        @show
        <!-- Control Sidebar -->

    </div>
    <!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->
@section('mainscripts')
    <!-- jQuery 3 -->
    <script src="{{ asset('assets/admin/bower_components/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{ asset('assets/admin/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('assets/admin/dist/js/adminlte.min.js') }}"></script>

    <!-- Optionally, you can add Slimscroll and FastClick plugins.
         Both of these plugins are recommended to enhance the
         user experience. -->
@show
</body>
</html>
