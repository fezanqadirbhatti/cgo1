<!-- Initialize Variables if NULL -->
<div style="display: none;">
    @if (empty($message))       {{ $message     = 'Forbidden' }} @endif
    @if (empty($description))   {{ $description = 'You are not allowed to access this page.' }} @endif
    @if (empty($title))         {{ $title       = 'Forbidden Access' }} @endif
    @if (empty($content))       {{ $content     = 'You do not have enough execute permissions...' }} @endif
</div>

@section('title', $title)
@section('admin-page-content')
    @parent
    <!-- Page heading -->
    @section('admin-page-heading', $message)
    <!-- Optional description -->
    @section('admin-page-description')
        {{ $description }}
    @endsection
    @section('admin-page-breadcrumb-home')
        <li><a href="javascript: void(0)"><i class="fa fa-fw fa-user-times"></i> {{ $message }} </a></li>
    @endsection
    @section('admin-page-breadcrumb') @endsection
    @section('page-content')
        @parent
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $description }}</strong>
        </div>
        <p> {!! $content !!}</p>
    @endsection
@endsection