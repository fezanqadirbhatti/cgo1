@section('admin-page-content')
    <!----- Content Wrapper ----->
    <div class="content-wrapper">
        <!-- Alert, Warning, Error Handling etc. -->
        <div class="row">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        <!-- END Alert, Warning etc. --->

        <!----- Content Header (Page header) ----------->
        <section class="content-header">
            <!-- Page Heading and Description -->
            <h1>
                @yield('admin-page-heading', 'Page Heading')
                <small> @yield('admin-page-description', 'Optional description') </small>
            </h1>
            <!-- Page Breadcrumb -->
            <ol class="breadcrumb">
              @section('admin-page-breadcrumb-home')
                @if(Auth::user()->hasRole('admin'))
                    <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home </a></li>
                @elseif(Auth::user()->hasRole('user'))
                    <li><a href="{{ route('userDashboard') }}"><i class="fa fa-home"></i> User </a></li>
                @endif
              @show
            @section('admin-page-breadcrumb')
                <li class="active">Here</li>
            @show
            </ol>
        </section>
        <!--- END Content Header ---->

        <!--- Page content --->
        <section class="content container-fluid">
            @yield('page-content')
        </section>
        <!-- /.content --->
    </div>
    <!------ END Content Wrapper ----->
@show