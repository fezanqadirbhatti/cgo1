<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>
    @include('includes.head')
</head>

<body class="home-three">

    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!--- PRELOADER -->
    <div class="preeloader">
        <div class="preloader-spinner"></div>
    </div>

    <!--SCROLL TO TOP-->
    <a href="#home" class="scrolltotop"><i class="fa fa-long-arrow-up"></i></a>

    <!--START TOP AREA-->
    <header class="top-area" id="home">
        @include('includes.header')
        <!--HOME SLIDER AREA-->
        @include('includes.slider')
        <!--END HOME SLIDER AREA-->
    </header>
    <!--END TOP AREA-->

    @section('404')
    @show

    <!--ABOUT AREA-->
    @section('about')
        @include('includes.sections.about')
    @show
    <!--ABOUT AREA END-->

    <!--ABOUT DETAIL AREA-->
    @section('about-detail')
        @include('includes.sections.about-detail')
    @show
    <!--ABOUT DETAIL AREA-->

    <!--FAQS AREA-->
    @section('faq')
        @include('includes.sections.faq')
    @show
    <!--FAQS AREA END-->

    <!--SERVICE AREA-->
    @section('service')
        @include('includes.sections.service')
    <!--SERVICE AREA END-->
    @show

    <!--PROMO AREA-->
    @section('promo')
        @include('includes.sections.promo')
    <!--PROMO AREA END-->
    @show

    <!--BLOG AREA-->
    @section('blog')
        @include('includes.sections.blog')
    <!--BLOG AREA END-->
    @show

    <!--TESTIMONIAL AREA -->
    @section('testimonial')
        @include('includes.sections.testimonial')
    <!--TESTIMONIAL AREA END -->
    @show

    <!--CLIENTS AREA-->
    @section('client')
        @include('includes.sections.client')
    <!--CLIENTS AREA END-->
    @show

    <!--CONTACT US AREA-->
    @section('contact')
        @include('includes.sections.contact')
    @show
    <!--CONTACT US AREA END-->

    <!--MAP AREA-->
    @section('map')
        @include('includes.sections.map')
    @show
    <!--MAP AREA END-->

    <!--PRICING AREA-->
    @section('pricing')
        @include('includes.sections.pricing')
    @show
    <!--PRICING AREA END-->

    <!--Quote FORM AREA-->
    @section('request-quote')
        @include('includes.sections.quote')
    @show
    <!--Quote FORM AREA-->

    <!--TRACKING-->
    @section('tracking')
        @include('includes.sections.tracking')
    @show
    <!--TRACKING-->

    <!--FOOER AREA-->
    @include('includes.footer')
    <!--FOOER AREA END-->

@section('lazyLoadScripts')
    <!--====== SCRIPTS JS ======-->
    <script src="{{ asset('assets/js/vendor/jquery-1.12.4.min.js') }}"></script>
    <script src="{{ asset('assets/js/vendor/bootstrap.min.js') }}"></script>
@show

@section('plugins')
    <!--====== PLUGINS JS ======-->
    <script src="{{ asset('assets/js/vendor/jquery.easing.1.3.js') }}"></script>
    <script src="{{ asset('assets/js/vendor/jquery-migrate-1.2.1.min.js') }}"></script>
    <script src="{{ asset('assets/js/vendor/jquery.appear.js') }}"></script>
    <script src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('assets/js/stellar.js') }}"></script>
    <script src="{{ asset('assets/js/wow.min.js') }}"></script>
    <script src="{{ asset('assets/js/stellarnav.min.js') }}"></script>
    <script src="{{ asset('assets/js/contact-form.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.sticky.js') }}"></script>
@show

@section('mainscripts')
    <!--===== ACTIVE JS=====-->
    <script src="{{ asset('assets/js/main.js') }}"></script>
@show

</body>

</html>
