@section('meta')
<!--====== USEFULL META ======-->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Transportation & Agency Template is a simple Smooth transportation and Agency Based Template" />
<meta name="keywords" content="Portfolio, Agency, Onepage, Html, Business, Blog, Parallax" />
@show

<!--====== TITLE TAG ======-->
<title> @yield('title') </title>

<!--====== FAVICON ICON =======-->
<link rel="shortcut icon" type="image/ico" href="{{ asset('assets/img/favicon.png') }}" />

@section('stylesheets')
<!--====== STYLESHEETS ======-->
<link rel="stylesheet" href="{{ asset('assets/css/normalize.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/stellarnav.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}">
@show

@section('mainstylesheets')
<!--====== MAIN STYLESHEETS ======-->
<link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
@show

@section('scripts')
<script src="{{ asset('assets/js/vendor/modernizr-2.8.3.min.js') }}"></script>
<!--[if lt IE 9]>
<script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
@show