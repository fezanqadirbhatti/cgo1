<div class="top-area-bg" data-stellar-background-ratio="0.6"></div>
<div class="header-top-area">
    <!--MAINMENU AREA-->
    <div class="mainmenu-area" id="mainmenu-area">
        <div class="mainmenu-area-bg"></div>
        <nav class="navbar">
            <div class="container">
                <div class="navbar-header">
                    <a href="{{ url('home') }}" class="navbar-brand"><img class="logo" src="{{ asset('assets/img/logo.png') }}" alt="logo"></a>
                </div>
                <div class="search-and-language-bar pull-right">
                    <ul>
                        <li class="search-box"><i class="fa fa-search"></i></li>
                        <li class="select-language">
                            <select name="#" id="#">
                                <option selected value="End">ENG</option>
                                <option value="ARA">ARA</option>
                                <option value="CHI">CHI</option>
                            </select>
                        </li>
                    </ul>
                    <form action="#" class="search-form">
                        <input type="search" name="search" id="search">
                        <button type="submit"><i class="fa fa-search"></i></button>
                    </form>
                </div>
                <div id="main-nav" class="stellarnav">
                    <ul id="nav" class="nav navbar-nav">
                        <li><a href="{{ url('') }}">home</a></li>
                        <li><a href="{{ url('about-us') }}">about</a>
                            <ul>
                                <li><a href="{{ url('about-us#company') }}">About Profile</a></li>
                                <li><a href="{{ url('about-us#history') }}">About History</a></li>
                                <li><a href="{{ url('about-us#report') }}">About Report</a></li>
                                <li><a href="{{ url('about-us#team') }}">About Team</a></li>
                                <li><a href="{{ url('about-us#support') }}">About Support</a></li>
                            </ul>
                        </li>
                        <li><a href="{{ url('services') }}">Service</a></li>
                        <li><a href="{{ url('pricing') }}">Pricing</a></li>
                        <li><a href="{{ url('request-quote') }}">Request A Quote</a></li>
                        <li><a href="{{ url('tracking') }}">Tracking</a></li>
                        <li><a href="{{ url('contact-us') }}">Contact</a></li>
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}"><i class="fa fa-user"></i></a></li>
                        @else
                            <li class="nav-item-user">
                                @if(Auth::user()->hasRole('admin'))
                                    <a id="username-item" href="{{ route('home') }}">
                                @elseif(Auth::user()->hasRole('user'))
                                    <a id="username-item" href="{{ route('userDashboard') }}">
                                @else
                                    <a id="username-item" href="javascript: void(0)">
                                @endif
                                    <i title="{{ Auth::user()->name }}" class="fa fa-user"> Hi! {{ Auth::user()->name }} </i>
                                </a>
                                <ul>
                                    <li>
                                        @if(Auth::user()->hasRole('admin'))
                                            <a href="{{ route('home') }}">
                                                {{ __('Dashboard') }}
                                        @elseif(Auth::user()->hasRole('user'))
                                            <a href="{{ route('userDashboard') }}">
                                                {{ __('User Home') }}
                                        @else
                                            <a href="javascript: void(0)">
                                                {{ 'Unknown' }}
                                        @endif
                                            </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit(); ">
                                            {{ __('Logout') }}
                                       </a>
                                   </li>
                                </ul>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <!--END MAINMENU AREA END-->
</div>