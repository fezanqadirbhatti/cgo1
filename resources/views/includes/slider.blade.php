<!--HOME SLIDER AREA-->
<div class="welcome-slider-area">
    <div class="welcome-single-slide slider-bg-one">
        <div class="container">
            <div class="row flex-v-center">
                <div class="col-md-10 col-md-offset-1">
                @section('slider-text')
                    <div class="welcome-text text-center">
                        <h1>WE MAKE STRONGEST SERVICE ABOVE THE WORLD</h1>
                        <p>We believes in providing personalized service to accommodate every client’s shipping needs. We have built a strong and respected name in the domestic freight forwarding industry</p>
                        <div class="home-button">
                            <a href="{{ url('services') }}">Our Service</a>
                            <a href="{{ url('request-quote') }}">Get A Quate</a>
                        </div>
                    </div>
                @show
                </div>
            </div>
        </div>
    </div>
    <div class="welcome-single-slide slider-bg-two">
        <div class="container">
            <div class="row flex-v-center">
                <div class="col-md-10 col-md-offset-1">
                @section('slider-text')
                    <div class="welcome-text text-center">
                        <h1>WE SUPPORT 24/7</h1>
                        <p>24 hour personalized service is what you can expect from CGO1. With our customer service representatives available 24 hours a day, 7 days a week.</p>
                        <div class="home-button">
                            <a href="{{ url('services') }}">Our Service</a>
                            <a href="{{ url('request-quote') }}">Get A Quate</a>
                        </div>
                    </div>
                @show
                </div>
            </div>
        </div>
    </div>
</div>
<!--END HOME SLIDER AREA-->