@section('404')
<section class="error-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                <div class="error-content text-center">
                    <img src="{{ asset('assets/img/404.png')  }}" alt="ERROR 404">
                    <h3>page not found</h3>
                    <h2> Opps! </h2>
                    <a href="{{ url('home') }}" class="read-more">Back To Home</a>
                </div>
            </div>
        </div>
    </div>
</section>
@show