@section('testimonial')
<!--TESTMONIAL AREA -->
<section class="testmonial-area gray-bg section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3 col-sm-12 col-xs-12">
                <div class="area-title text-center wow fadeIn">
                    <h2>what client’s say</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4 col-sm-8 col-sm-offset-2 col-xs-8 col-xs-offset-2">
                <div class="client-photo-list wow fadeIn">
                    <div class="client_photo">
                        <div class="item">
                            <img src="{{ asset('assets/img/testmonial/1.jpg') }}" alt="">
                        </div>
                        <div class="item">
                            <img src="{{ asset('assets/img/testmonial/2.jpg') }}" alt="">
                        </div>
                        <div class="item">
                            <img src="{{ asset('assets/img/testmonial/3.jpg') }}" alt="">
                        </div>
                        <div class="item">
                            <img src="{{ asset('assets/img/testmonial/1.jpg') }}" alt="">
                        </div>
                        <div class="item">
                            <img src="{{ asset('assets/img/testmonial/2.jpg') }}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="client_nav">
                    <span class="fa fa-angle-left testi_prev"></span>
                    <span class="fa fa-angle-right testi_next"></span>
                </div>
            </div>
            <div class="col-xs-12 col-md-10 col-md-offset-1 text-center">
                <div class="client-details-content wow fadeIn">
                    <div class="client_details">
                        <div class="item">
                            <q>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </q>
                            <h3>JABIN KANE</h3>
                            <p>CEO, TOPSMMPANEL.COM</p>
                        </div>
                        <div class="item">
                            <q>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </q>
                            <h3>JABIN KANE</h3>
                            <p>CEO, TOPSMMPANEL.COM</p>
                        </div>
                        <div class="item">
                            <q>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </q>
                            <h3>JABIN KANE</h3>
                            <p>CEO, TOPSMMPANEL.COM</p>
                        </div>
                        <div class="item">
                            <q>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </q>
                            <h3>JABIN KANE</h3>
                            <p>CEO, TOPSMMPANEL.COM</p>
                        </div>
                        <div class="item">
                            <q>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </q>
                            <h3>JABIN KANE</h3>
                            <p>CEO, TOPSMMPANEL.COM</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--TESTMONIAL AREA END -->
@show