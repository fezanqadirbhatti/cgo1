@section('promo')
<!--PROMO AREA-->
<section class="promo-area">
    <div class="promo-bottom-area section-padding">
        <div class="promo-botton-area-bg" data-stellar-background-ratio="0.6"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1 col-sm-12 col-xs-12 text-center">
                    <div class="promo-bottom-content wow fadeIn">
                        <h2>we provide international freight &amp; logistics service worldwidw</h2>
                        <a href="#" class="read-more">Get a quote</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--PROMO AREA END-->
@show