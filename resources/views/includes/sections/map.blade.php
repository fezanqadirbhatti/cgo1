@section('map')
    <section>
        <div class="map-area relative">
            <div id="map" style="width: 100%; height: 400px;"> </div>
        </div>
    </section>
@show