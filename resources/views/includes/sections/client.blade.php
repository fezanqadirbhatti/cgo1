@section('client')
<!--CLIENTS AREA-->
<section class="clients-area section-padding wow fadeIn">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                <div class="client-list">
                    <div class="single-client">
                        <img src="{{ asset('assets/img/client/1.jpg') }}" alt="">
                    </div>
                    <div class="single-client">
                        <img src="{{ asset('assets/img/client/2.jpg') }}" alt="">
                    </div>
                    <div class="single-client">
                        <img src="{{ asset('assets/img/client/3.jpg') }}" alt="">
                    </div>
                    <div class="single-client">
                        <img src="{{ asset('assets/img/client/4.jpg') }}" alt="">
                    </div>
                    <div class="single-client">
                        <img src="{{ asset('assets/img/client/5.jpg') }}" alt="">
                    </div>
                    <div class="single-client">
                        <img src="{{ asset('assets/img/client/1.jpg') }}" alt="">
                    </div>
                    <div class="single-client">
                        <img src="{{ asset('assets/img/client/2.jpg') }}" alt="">
                    </div>
                    <div class="single-client">
                        <img src="{{ asset('assets/img/client/3.jpg') }}" alt="">
                    </div>
                    <div class="single-client">
                        <img src="{{ asset('assets/img/client/4.jpg') }}" alt="">
                    </div>
                    <div class="single-client">
                        <img src="{{ asset('assets/img/client/5.jpg') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--CLIENTS AREA END-->
@show