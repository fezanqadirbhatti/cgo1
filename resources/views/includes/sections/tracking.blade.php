@section('tracking')
    <section class="section-padding tracking-area" id="tracking">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="background-color:#eef1f5; font-size:18px;">
                            Track Your Shipment
                        </div>
                        <div class="row media" style="padding:30px 30px 30px 30px;">
                            <div class="text-center col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                <h4 class="media-heading">Enter Your Tracking Code</h4>
                                <form action="" method="POST" name="tracking" id="tracking" class="form-inline form-track">
                                    <p class="width-full">
                                        <input id="tracking_id" maxlength="60" name="tracking_id" type="text" required="" placeholder="Enter Tracking CODE..">
                                        <input class="btn btn-primary" name="commit" id="commit" type="submit" value="Track!">
                                    </p>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@show