@section('about')
<!--ABOUT AREA-->
<section class="about-area gray-bg section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3 col-sm-12 col-xs-12">
                <div class="area-title text-center wow fadeIn">
                    <h2>Welcome to CGO1</h2>
                    <p>CGO1 is your solution for all ground freight handling across Pakistan.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5 col-lg-5 col-sm-12 col-xs-12">
                <div class="about-left-content-area wow fadeIn">
                    <img src="{{ asset('assets/img/about/about-cargo.png') }}" alt="">
                    <p>At Say Cargo Express, we believe in making a positive difference at every turn, doing what we can to help protect the environment for future generations.</p>
                </div>
            </div>
            <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
                <div class="about-content-area wow fadeIn">
                    <div class="about-content">
                        <h2>We have years experience in this passion</h2>
                        <p>Collectively and individually, everyone at CGO1 has embraced an eco-friendly mindset for our company that’s both good for the environment and smart for business.</p>
                        <a href="#">read more <i class="fa fa-angle-right"></i></a>
                    </div>
                    <div class="about-count">
                        <div class="single-about-count">
                            <h4><i class="fa fa-suitcase"></i> 120</h4>
                            <p>Project Done</p>
                        </div>
                        <div class="single-about-count">
                            <h4><i class="fa fa-thumbs-o-up"></i> 100</h4>
                            <p>Project Done</p>
                        </div>
                        <div class="single-about-count">
                            <h4><i class="fa fa-users"></i> 30</h4>
                            <p>Project Done</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--ABOUT AREA END-->
@show