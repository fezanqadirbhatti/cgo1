@section('pricing')
<section class="section-padding pricing-area" id="pricing">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3 col-sm-12 col-xs-12">
                <div class="area-title text-center wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
                    <h2>Pricing</h2>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, </p>
                </div>
            </div>
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                <table class="table table-hover table-responsive">
                    <thead class="thead-light">
                        <tr>
                            <th>DCS City Express</th>
                            <th>Rates (PKR)</th>
                            <th>Remarks</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>0 to 0.5 kg</td>
                            <td>50.00</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>0.5 to 1 Kg</td>
                            <td>50.00</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Each Additional Kg</td>
                            <td>35/=</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Special Handling charges</td>
                            <td>100.00</td>
                            <td>Consignment</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td class="alert-info" colspan="3">( Same Day & Holiday Delivery )	</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>
@show