<section class="faqs-area section-padding wow fadeIn">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                <div class="faqs-left-img">
                    <img src="{{asset( 'assets/img/about/about-details.jpg' )}}" alt="">
                </div>
            </div>
            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                <div class="faqs-list">
                    <h3>why choose <span>carries transportation ?</span></h3>
                    <div class="panel-group" id="accordion">
                        <div class="panel active">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#faqs_one"><i class="fa fa-minus"></i> Lorem Ipsum is simply dummy text ?</a>
                                </h4>
                            </div>
                            <div id="faqs_one" class="panel-collapse collapse in">
                                <div class="panel-body">Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est.</div>
                            </div>
                        </div>
                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#faqs_two"><i class="fa fa-minus"></i> Lorem Ipsum is simply dummy text ?</a>
                                </h4>
                            </div>
                            <div id="faqs_two" class="panel-collapse collapse">
                                <div class="panel-body">Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est.</div>
                            </div>
                        </div>
                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#faqs_three"><i class="fa fa-minus"></i> Lorem Ipsum is simply dummy text ?</a>
                                </h4>
                            </div>
                            <div id="faqs_three" class="panel-collapse collapse">
                                <div class="panel-body">Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est.</div>
                            </div>
                        </div>
                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#faqs_four"><i class="fa fa-minus"></i> Lorem Ipsum is simply dummy text ?</a>
                                </h4>
                            </div>
                            <div id="faqs_four" class="panel-collapse collapse">
                                <div class="panel-body">Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>