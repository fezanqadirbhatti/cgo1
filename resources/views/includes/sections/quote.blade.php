@section('request-quote')
<section class="section-padding quote-form-area wow fadeIn" id="quote">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                <div class="quote-form-area wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
                    <h3>Request A Quote</h3>
                    <form class="quote-form" action="#">
                        <p class="width-full">
                            <input type="text" name="name" id="name" placeholder="Your Name">
                        </p>
                        <p class="width-half">
                            <input type="email" name="email" id="email" placeholder="Email">
                            <input class="pull-right" type="phone" name="phone" id="phone" placeholder="Phone">
                        </p>
                        <p class="width-half">
                            <input type="text" name="type" id="type" placeholder="Type">
                            <input class="pull-right" type="text" name="quantity" id="quantity" placeholder="Quantity">
                        </p>
                        <p class="width-full">
                            <input type="text" name="destination" id="destination" placeholder="Destination">
                        </p>
                        <p>
                            <textarea name="quote-message" id="quote-message" cols="30" rows="4" placeholder="Your Message..."></textarea>
                        </p>
                        <button type="submit">Send</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@show