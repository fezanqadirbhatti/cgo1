@section('blog')
<!--BLOG AREA-->
<section class="blog-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3 col-sm-12 col-xs-12">
                <div class="area-title text-center wow fadeIn">
                    <h2>Latest Blog</h2>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                <div class="single-blog wow fadeInUp" data-wow-delay="0.2s">
                    <div class="blog-image">
                        <img src="{{ asset('assets/img/blog/blog_1.jpg') }}" alt="">
                    </div>
                    <div class="blog-details text-center">
                        <div class="blog-meta"><a href="#"><i class="fa fa-ship"></i></a></div>
                        <h3><a href="single-blog.html">Ocean Freight</a></h3>
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout..</p>
                        <a href="single-blog.html" class="read-more">Read More</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                <div class="single-blog wow fadeInUp" data-wow-delay="0.3s">
                    <div class="blog-image">
                        <img src="{{ asset('assets/img/blog/blog_2.jpg') }}" alt="">
                    </div>
                    <div class="blog-details text-center">
                        <div class="blog-meta"><a href="#"><i class="fa fa-plane"></i></a></div>
                        <h3><a href="single-blog.html">Air Freight</a></h3>
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout..</p>
                        <a href="single-blog.html" class="read-more">Read More</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                <div class="single-blog wow fadeInUp" data-wow-delay="0.4s">
                    <div class="blog-image">
                        <img src="{{ asset('assets/img/blog/blog_3.jpg') }}" alt="">
                    </div>
                    <div class="blog-details text-center">
                        <div class="blog-meta"><a href="#"><i class="fa fa-truck"></i></a></div>
                        <h3><a href="single-blog.html">Street Freight</a></h3>
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout..</p>
                        <a href="single-blog.html" class="read-more">Read More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--BLOG AREA END-->
@show