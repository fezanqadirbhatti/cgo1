@extends('layouts.default')
@section('title', '404 page not found')

@section('slider-text')
    <div class="welcome-text text-center">
        <h2>ERROR 404</h2>
        <p>Page not found</p>
        <div class="home-button">
            <a href="{{ url('') }}">Home</a>
        </div>
    </div>
@endsection

<!--404-->
@section('404')
    @include('includes.sections.404')
@endsection
<!--404-->

<!-- Excluded Sections -->
@section('pricing')
@endsection

@section('about')
@endsection

@section('service')
@show

@section('promo')
@endsection

@section('blog')
@endsection

@section('testimonial')
@endsection

@section('client')
@endsection

@section('faq')
@endsection

@section('about-detail')
@endsection

@section('contact')
@endsection

@section('map')
@endsection

@section('request-quote')
@endsection

@section('tracking')
@endsection
<!-- Excluded Sections -->
