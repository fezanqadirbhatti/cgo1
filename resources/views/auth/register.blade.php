@extends('layouts.admin.auth.default')
@section('title', 'Registration')

@section('bodyStart')
    <body class="hold-transition login-page">
@endsection

@section('auth-content-section')
    @parent
    @section('email-verification')
    @endsection
    @section('email-confirm')
    @endsection
    @section('auth-login')
    @endsection
    @section('auth-reset-email')
    @endsection
    @section('auth-reset')
    @endsection
    @section('auth-lockscreen')
    @endsection
@endsection