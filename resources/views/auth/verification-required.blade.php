@extends('layouts.admin.auth.default')
@section('title', 'Activation Required')

@section('bodyStart')
    <body class="hold-transition login-page">
@endsection

@section('auth-content-section')
    @parent
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
            @if(!empty($status))
                <div class="alert alert-success alert-dismissible ">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i> {{ $status }}</h4>
                    {{ $message }}
                </div>
            @endif
                <div class="panel panel-default">
                    <div class="panel-heading">{{ Lang::get('titles.activation') }}</div>
                    <div class="panel-body">
                        <p>{{ Lang::get('auth.regThanks') }}</p>
                        <p>{{ Lang::get('auth.anEmailWasSent',['email' => $email, 'date' => $date ] ) }}</p>
                        <p>{{ Lang::get('auth.clickInEmail') }}</p>
                        <p><a href='{{ route('resend-activation') }}' class="btn btn-primary">{{ Lang::get('auth.clickHereResend') }}</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @section('email-verification')
    @endsection
    @section('email-confirm')
    @endsection
    @section('auth-login')
    @endsection
    @section('auth-register')
    @endsection
    @section('auth-reset-email')
    @endsection
    @section('auth-reset')
    @endsection
    @section('auth-lockscreen')
    @endsection
@endsection