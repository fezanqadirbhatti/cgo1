@extends('layouts.admin.auth.default')
@section('title', 'Password Reset Email')

@section('bodyStart')
    <body class="hold-transition login-page">
@endsection

@section('auth-content-section')
    @parent
    @section('email-verification')
    @endsection
    @section('email-confirm')
    @endsection
    @section('auth-login')
    @endsection
    @section('auth-register')
    @endsection
    @section('auth-reset')
    @endsection
    @section('auth-lockscreen')
    @endsection
@endsection