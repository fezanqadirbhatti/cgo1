@extends('layouts.admin.auth.default')
@section('title', 'Reset Password')

@section('bodyStart')
    <body class="hold-transition login-page">
@endsection

@section('token', $token)

@section('auth-content-section')
    @parent
    @section('email-verification')
    @endsection
    @section('email-confirm')
    @endsection
    @section('auth-login')
    @endsection
    @section('auth-register')
    @endsection
    @section('auth-reset-email')
    @endsection
    @section('auth-lockscreen')
    @endsection
@endsection