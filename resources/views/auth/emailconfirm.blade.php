@extends('layouts.admin.auth.default')
@section('title', 'Email Confirmed')

@section('bodyStart')
    <body class="hold-transition login-page">
@endsection

@section('auth-content-section')
    @parent
    @section('email-verification')
    @endsection
    @section('auth-register')
    @endsection
    @section('auth-login')
    @endsection
    @section('auth-reset-email')
    @endsection
    @section('auth-reset')
    @endsection
    @section('auth-lockscreen')
    @endsection
@endsection