<!-- Initialize Variables if NULL -->
<div style="display: none;">
    @if (empty($roles)) {{ $roles = '' }} @endif
    @if (empty($title)) {{ $title = '' }} @endif
</div>

@extends('layouts.admin.default')

@if (Auth::user()->authorizeRoles($roles))
    @section('title', $title)
    @section('admin-page-content')
        @parent
        <!-- Page heading -->
        @section('admin-page-heading', 'User Profile')
        <!-- Optional description -->
        @section('admin-page-description')
            A user profile is a visual display of personal
            data associated with user,
            or a customized desktop environment.
        @endsection
        @section('admin-page-breadcrumb')
            <li class="active"><a href="{{ route('userProfile') }}"> <i class="fa fa-user-circle"></i> Profile </a></li>
        @endsection
        @section('page-content')
            @parent
            <!-- Page content -->
            <section class="content">
                <div class="row">
                    <div class="col-md-3">
                        <!-- Profile Image -->
                        <div class="box box-primary">
                            <div class="box-body box-profile">
                                <img class="profile-user-img img-responsive img-circle" src="{{ url('storage/avatars') .'/'. Auth::user()->avatar }}" alt="User profile picture">
                                <h3 class="profile-username text-center">{{ Auth::user()->name }}</h3>
                                <p class="text-muted text-center">Software Engineer</p>
                                <div class="box-header with-border">
                                    <h3 class="box-title">About Me</h3>
                                </div>
                                <div class="box-body">
                                    <strong><i class="fa fa-home margin-r-5"></i> Address </strong>
                                    <p class="text-muted">
                                        B.S. in Computer Science from the Virtual University of Pakistan
                                    </p>
                                    <hr>
                                    <strong><i class="fa fa-phone margin-r-5"></i> Phone </strong>
                                    <p class="text-muted">
                                        +92-3101414959
                                    </p>
                                    <hr>
                                    <strong><i class="fa fa-pencil margin-r-5"></i> About me</strong>
                                    <p>
                                        I am a Computer Science Graduate.
                                        I had worked in different technologies
                                        but web application development is my specialization.
                                    </p>
                                    <hr>
                                    <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes </strong>
                                    <p>
                                        I am a Computer Science Graduate.
                                        I had worked in different technologies
                                        but web application development is my specialization.
                                    </p>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <div class="col-md-9">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li><a href="javascript: void(0)"><h4>Update Profile</h4></a></li>
                            </ul>
                            <div class="profile-update-box">
                                <div class="" id="settings">
                                    <form action="{{ route('updateProfile') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                        @csrf
                                        <div class="form-group">
                                            <label for="inputName" class="col-sm-2 control-label">Full Name</label>
                                            <div class="col-sm-10">
                                                <input type="text" value="{{ Auth::user()->name }}" class="form-control" id="inputName" placeholder="Name">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="designation" class="col-sm-2 control-label">Designation</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="designation" placeholder="Designation">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="phone" class="col-sm-2 control-label">Phone</label>
                                            <div class="col-sm-10">
                                                <input type="tel" class="form-control" id="phone" placeholder="Phone">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Address" class="col-sm-2 control-label">Address</label>
                                            <div class="col-sm-10">
                                                <textarea class="form-control" id="Address" placeholder="Address"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Gender" class="col-sm-2 control-label">Gender</label>
                                            <div class="col-sm-10">
                                                <select class="form-control">
                                                    <option value="1">Male</option>
                                                    <option value="2">Female</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputProfileFile" class="col-sm-2 control-label">Profile Picture</label>
                                            <div class="col-sm-10">
                                                <input type="file" class="form-control-file" name="avatar" id="avatarFile" aria-describedby="fileHelp">
                                                <small id="fileHelp" class="form-text text-muted help-block">Please upload a valid image file. Size of image should not be more than 2MB.</small>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <button type="submit" class="btn btn-primary">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div>
                        <!-- /.nav-tabs-custom -->
                    </div>
                </div>
            </section>
        @endsection
    @endsection
@else
    @include('layouts.admin.errors.access-denied')
@endif