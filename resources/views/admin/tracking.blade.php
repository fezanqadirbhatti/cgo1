<!-- Initialize Variables if NULL -->
<div style="display: none;">
    @if (empty($roles)) {{ $roles = '' }} @endif
    @if (empty($title)) {{ $title = '' }} @endif
</div>

@extends('layouts.admin.default')

@if (Auth::user()->authorizeRoles($roles))
    @section('title', $title)
    @section('admin-page-content')
        @parent
        <!-- Page heading -->
        @section('admin-page-heading', 'Consignment Tracking')
        <!-- Optional description -->
        @section('admin-page-description')
            Track your consignments.
        @endsection
        @section('admin-page-breadcrumb-home')
            <li><a href="{{ route('userDashboard') }}"> <i class="fa fa-home"></i> User </a></li>
        @endsection
        @section('admin-page-breadcrumb')
            <li class="active"><a href="{{ route('tracking') }}"> <i class="fa fa-truck"></i> Consignment Tracking </a></li>
        @endsection
        @section('page-content')
            @parent
            <p> Enter your HouseBill #, your PO# or your Ref# for updated tracking information. </p>
            <div class="login-box">
                <!-- /.login-logo -->
                <div class="login-box-body">
                    <p class="login-box-msg">{{ __('SHIPMENT TRACKING') }}</p>

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('tracking') }}">
                        @csrf
                        <div class="form-group has-feedback">
                            <input id="tracking" type="number" class="form-control{{ $errors->has('number') ? ' is-invalid' : '' }}" placeholder="Tracking ID" name="email" value="{{ old('tracking') }}" required>
                            <span class="glyphicon glyphicon-road form-control-feedback"></span>
                            @if ($errors->has('tracking'))
                                <span class="invalid-credentials">
                                    <strong>{{ $errors->first('tracking') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="row">
                            <!-- /.col -->
                            <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Track') }}
                                </button>
                            </div>
                            <!-- /.col -->
                        </div>
                    </form>
                </div>
                <!-- /.login-box-body -->
            </div>
            <!-- Reset Password Section -->
        @endsection
    @endsection
@else
    @include('layouts.admin.errors.access-denied')
@endif