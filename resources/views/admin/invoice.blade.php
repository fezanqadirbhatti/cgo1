<!-- Initialize Variables if NULL -->
<div style="display: none;">
    @if (empty($roles)) {{ $roles = '' }} @endif
    @if (empty($title)) {{ $title = '' }} @endif
</div>

@extends('layouts.admin.default')

@if (Auth::user()->authorizeRoles($roles))
    @section('title', $title)
    @section('admin-page-content')
        @parent
        <!-- Page heading -->
        @section('admin-page-heading', 'Invoices')
        <!-- Optional description -->
        @section('admin-page-description')
            Get all your customer invoices.
        @endsection
        @section('admin-page-breadcrumb-home')
            <li><a href="{{ route('userDashboard') }}"> <i class="fa fa-home"></i> User </a></li>
        @endsection
        @section('admin-page-breadcrumb')
            <li class="active"><a href="{{ route('invoice') }}"> <i class="fa fa-list"></i> Invoices </a></li>
        @endsection
        @section('page-content')
            @parent
            <p>In this page you can list your customer invoices using search filters.</p>
        @endsection
    @endsection
@else
    @include('layouts.admin.errors.access-denied')
@endif