<!-- Initialize Variables if NULL -->
<div style="display: none;">
    @if (empty($roles)) {{ $roles = '' }} @endif
    @if (empty($title)) {{ $title = '' }} @endif
</div>

@extends('layouts.admin.default')

@if (Auth::user()->authorizeRoles($roles))
    @section('title', $title)
    @section('admin-page-content')
        @parent
        <!-- Page heading -->
        @section('admin-page-heading', 'Dashboard')
        <!-- Optional description -->
        @section('admin-page-description')
            Dashboards often provide at-a-glance views of KPIs (key performance indicators) of the business process.
        @endsection
        @section('admin-page-breadcrumb-home')
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Dashboard </a></li>
        @endsection
        @section('admin-page-breadcrumb')
            <li class="active">Home</li>
        @endsection
        @section('page-content')
            @parent
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Mauris id nulla tempus, mattis dolor nec, imperdiet tortor.
                Suspendisse at nibh pellentesque nulla blandit egestas.
                Maecenas nec massa lacus. Nulla mattis et augue et condimentum.
                Ut et nibh vulputate, aliquet mauris vel, lacinia sem.
                Morbi eget magna consectetur, consectetur leo et, blandit orci.
                Donec congue felis ut suscipit volutpat.
                Ut quis commodo massa, at porttitor velit.
                Nunc ultricies dictum ante in pulvinar.
                Vivamus sit amet tortor non enim tempus vulputate eu ut purus.
                Nunc laoreet a justo in bibendum.
                Quisque finibus quam quam, eu feugiat erat sagittis nec.
                Maecenas eleifend sagittis arcu, vel efficitur nibh maximus consectetur.
                Pellentesque vehicula facilisis vulputate.
                Nam malesuada urna tempus lorem congue porta.
            </p>
        @endsection
    @endsection
@else
    @include('layouts.admin.errors.access-denied')
@endif