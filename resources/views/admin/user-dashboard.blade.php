<!-- Initialize Variables if NULL -->
<div style="display: none;">
    @if (empty($roles)) {{ $roles = '' }} @endif
    @if (empty($title)) {{ $title = '' }} @endif
</div>

@extends('layouts.admin.default')

@if (Auth::user()->authorizeRoles($roles))
    @section('title', $title)
    @section('admin-page-content')
        @parent
        <!-- Page heading -->
        @section('admin-page-heading', 'Home')
        <!-- Optional description -->
        @section('admin-page-description')
            User home with limited access.
        @endsection
        @section('admin-page-breadcrumb-home')
            <li><a href="{{ route('userDashboard') }}"><i class="fa fa-home"></i> User </a></li>
        @endsection
        @section('admin-page-breadcrumb')
            <li class="active"><a href="{{ route('userDashboard') }}"> Home </a></li>
        @endsection
        @section('page-content')
            @parent
            <p>This is authenticated user home area.</p>
            <p>A user has assigned limited rights which depends on user role.</p>
            <p>A user may request Admin to grant access on more resources.</p>
        @endsection
    @endsection
@else
    @include('layouts.admin.errors.access-denied')
@endif