@extends('layouts.admin.auth.default')
@section('title', 'Lockscreen')
@section('auth-head-section')
    @parent
    @section('lock-script')
        <script>
            window.Lockscreen = {
                locked: "{{ session()->get('lockscreen', false) }}",
                route: "{{ url('/lockscreen') }}",
            };
        </script>
    @endsection
@show

@section('bodyStart')
    <body class="hold-transition lockscreen">
@endsection

@section('auth-content-section')
    @parent
    @section('loginurl')
        {{ route('login') }}
    @endsection

    @section('username')
        {{ Auth::user()->name }}
    @endsection

    @section('user-profile-pic')
        {{ asset('assets/admin/dist/img/profile') }}/{{ Auth::user()->avatar }}
    @endsection

    @section('auth-login')
    @endsection
    @section('auth-register')
    @endsection
    @section('auth-reset-email')
    @endsection
    @section('auth-reset')
    @endsection
@endsection