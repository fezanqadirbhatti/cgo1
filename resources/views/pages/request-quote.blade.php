@extends('layouts.default')
@section('title', 'Request Quote')

@section('request-quote')
    @parent
@endsection

<!-- Excluded Sections -->
@section('about')
@endsection

@section('service')
@endsection

@section('promo')
@endsection

@section('blog')
@endsection

@section('testimonial')
@endsection

@section('client')
@endsection

@section('faq')
@endsection

@section('about-detail')
@endsection

@section('contact')
@endsection

@section('map')
@endsection

@section('pricing')
@endsection

@section('tracking')
@endsection

@section('tracking')
@endsection
<!-- Excluded Sections -->