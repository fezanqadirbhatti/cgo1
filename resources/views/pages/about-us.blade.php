@extends('layouts.default')
@section('title', 'Abour Us')

@section('about')
    @parent
@endsection

@section('about-detail')
    @parent
@endsection

@section('faq')
    @parent
@endsection

@section('promo')
    @parent
@endsection

@section('testimonial')
    @parent
@endsection

@section('client')
    @parent
@endsection

<!-- Excluded Sections -->
@section('service')
@endsection

@section('blog')
@endsection

@section('contact')
@endsection

@section('map')
@endsection

@section('request-quote')
@endsection

@section('pricing')
@endsection

@section('tracking')
@endsection
<!-- Excluded Sections -->