@extends('layouts.default')
@section('title', 'Track Your Shipment')

@section('tracking')
    @parent
@endsection

<!-- Excluded Sections -->
@section('about')
@endsection

@section('service')
@endsection

@section('promo')
@endsection

@section('blog')
@endsection

@section('testimonial')
@endsection

@section('client')
@endsection

@section('faq')
@endsection

@section('about-detail')
@endsection

@section('contact')
@endsection

@section('map')
@endsection

@section('pricing')
@endsection

@section('request-quote')
@endsection
<!-- Excluded Sections -->