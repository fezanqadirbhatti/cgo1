@extends('layouts.default')
@section('title', 'Contact us')

@section('contact')
    @parent
@endsection

@section('map')
    @parent
@endsection

@section('mainscripts')
    @parent
    <script>
        function initMap() {
            var uluru = {lat: 31.567335, lng: 74.413627};
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 16,
                center: uluru
            });
            var marker = new google.maps.Marker({
                position: uluru,
                map: map
            });
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key={{config('services.google.map.api-key')}}&callback=initMap">
    </script>
@endsection

<!-- Excluded Sections -->
@section('about')
@endsection

@section('service')
@show

@section('promo')
@endsection

@section('blog')
@endsection

@section('testimonial')
@endsection

@section('client')
@endsection

@section('faq')
@endsection

@section('about-detail')
@endsection

@section('pricing')
@endsection

@section('request-quote')
@endsection

@section('tracking')
@endsection
<!-- Excluded Sections -->
