@extends('layouts.default')
@section('title', 'Home Page')

@section('service')
    @parent
@endsection

@section('promo')
    @parent
@endsection

@section('blog')
    @parent
@endsection

@section('testimonial')
    @parent
@endsection

@section('client')
    @parent
@endsection
<!-- Excluded Sections -->
@section('about')
@endsection

@section('about-detail')
@show

@section('about-detail')
@endsection

@section('contact')
@endsection

@section('faq')
@endsection

@section('map')
@endsection

@section('request-quote')
@endsection

@section('pricing')
@endsection

@section('tracking')
@endsection
<!-- Excluded Sections -->