@extends('layouts.default')
@section('title', 'Pricing')

@section('pricing')
    @parent
@endsection


<!-- Excluded Sections -->
@section('about')
@endsection

@section('service')
@show

@section('promo')
@endsection

@section('blog')
@endsection

@section('testimonial')
@endsection

@section('client')
@endsection

@section('faq')
@endsection

@section('about-detail')
@endsection

@section('contact')
@endsection

@section('map')
@endsection

@section('request-quote')
@endsection

@section('tracking')
@endsection
<!-- Excluded Sections -->
