@extends('layouts.default')
@section('title', 'Services')

@section('about')
    @parent
@endsection

@section('service')
    @parent
@endsection

@section('testimonial')
    @parent
@endsection

@section('client')
    @parent
@endsection

<!-- Excluded Sections -->
@section('blog')
@endsection

@section('contact')
@endsection

@section('promo')
@endsection

@section('about-detail')
@endsection

@section('faq')
@endsection

@section('map')
@endsection

@section('request-quote')
@endsection

@section('pricing')
@endsection

@section('tracking')
@endsection
<!-- Excluded Sections -->