<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Session;
use App\Traits\LockscreenMethods;

class LockscreenController extends Controller {
	use LockscreenMethods;

    public function lockscreen(){
        return view('admin.lockscreen');
    }

    public function showUnlockForm(){
        return view('admin.home');
    }
}