<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Storage;

class AdminUserProfileController extends AdminController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $roles = ['user', 'admin'];
        $pageTitle = 'User Profile';
        if ($request->user()->authorizeRoles($roles)) {
            return view('admin.user.profile', [
                'roles' => $roles,
                'title' => $pageTitle,
            ]);
        } else {
            return view('admin.user.profile', [
                'title'         => 'Forbidden Access.',
                'message'       => '403: Forbidden',
                'description'   => 'You are not allowed to access this page.',
                'content'       => 'You do not have enough execute permissions. <br>  
                                    For example, you may receive this error message if you try to access 
                                    a PHP page in a directory where permissions are set to None.',
            ]);
        }
    }

    public function updateProfile(Request $request)
    {
        $request->validate([
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $user = Auth::user();

        $avatarName = $user->id.'_avatar'.time().'.'.request()->avatar->getClientOriginalExtension();

        $request->avatar->storeAs('avatars',$avatarName);
        $user->avatar = $avatarName;
        $user->save();

        return back()
            ->with('success','Your profile has been updated successfully.');
    }
}
