<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\AdminController;

class AdminUserDashboardController extends AdminController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $roles = ['user'];
        $pageTitle = 'User Home';
        if ($request->user()->authorizeRoles($roles)) {
            return view('admin.user-dashboard', [
                'roles'     => $roles,
                'title'     => $pageTitle,
                'verified'  =>  $user->verified
            ]);
        } else {
            return view('admin.user-dashboard', [
                'verified'      =>  $user->verified,
                'title'         => 'Forbidden Access.',
                'message'       => '403: Forbidden',
                'description'   => 'You are not allowed to access this page.',
                'content'       => 'You do not have enough execute permissions. <br>  
                                    For example, you may receive this error message if you try to access 
                                    a PHP page in a directory where permissions are set to None.',
            ]);
        }
    }
}