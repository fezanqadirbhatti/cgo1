<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use App\Jobs\SendVerificationEmail;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

class ActivateController extends Controller
{
    private static $userHomeRoute  = 'userDashboard';
    private static $adminHomeRoute = 'home';
    private static $activationView = 'auth.verification-required';
    private static $activationRoute = 'activation-required';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public static function getUserHomeRoute()
    {
        return self::$userHomeRoute;
    }

    public static function getAdminHomeRoute()
    {
        return self::$adminHomeRoute;
    }

    public static function getActivationView()
    {
        return self::$activationView;
    }

    public static function getActivationRoute()
    {
        return self::$activationRoute;
    }

    public static function activeRedirect($user, $currentRoute)
    {
        if ($user->verified) {
            Log::info('Activated user attempted to visit '.$currentRoute.'. ', [$user]);

            if($user->authorizeRoles(['admin'])) {
                return redirect()->route(self::getAdminHomeRoute())
                    ->with('status', 'info')
                    ->with('message', trans('auth.alreadyActivated'));
            }
            return redirect()->route(self::getUserHomeRoute())
                ->with('status', 'info')
                ->with('message', trans('auth.alreadyActivated'));
        }

        return false;
    }

    public function initial()
    {
        $user = Auth::user();
        $lastActivation = Activation::where('user_id', $user->id)->get()->last();
        $currentRoute = Route::currentRouteName();

        $rCheck = $this->activeRedirect($user, $currentRoute);
        if ($rCheck) {
            return $rCheck;
        }

        $data = [
            'email' => $user->email,
            'date'  => $lastActivation->created_at->format('m/d/Y'),
        ];

        return view($this->getActivationView())->with($data);
    }

    public function activationRequired()
    {
        $user = Auth::user();
        $currentRoute = Route::currentRouteName();

        if ($user->verified == 1) {
            return $this->activeRedirect($user, $currentRoute)
                ->with('status', 'info')
                ->with('message', trans('auth.alreadyActivated'));
        }

        $data = [
            'email'     => $user->email,
            'date'      => $user->created_at->format('m/d/Y'),
            'status'    => Session::get('status'),
            'message'   => Session::get('message'),
        ];

        return view($this->getActivationView())->with($data);
    }

    public function resend()
    {
        $user = Auth::user();
        $currentRoute = Route::currentRouteName();
        if ($user->verified == 0) {

            dispatch(new SendVerificationEmail($user));

            Log::info('Activation resent to registered user. '.$currentRoute.'. ', [$user]);

            return redirect()->route(self::getActivationRoute())
                ->with('status', 'success')
                ->with('message', trans('auth.activationSent'));
        }

        Log::info('Activated user attempted to navigate to '.$currentRoute.'. ', [$user]);

        return $this->activeRedirect($user, $currentRoute)
            ->with('status', 'info')
            ->with('message', trans('auth.alreadyActivated'));
    }

    public function exceeded()
    {
        $user = Auth::user();
        $currentRoute = Route::currentRouteName();
        $timePeriod = config('settings.timePeriod');
        $lastActivation = Activation::where('user_id', $user->id)->get()->last();
        $activationsCount = Activation::where('user_id', $user->id)
            ->where('created_at', '>=', Carbon::now()->subHours($timePeriod))
            ->count();

        if ($activationsCount >= config('settings.maxAttempts')) {
            Log::info('Locked non-activated user attempted to visit '.$currentRoute.'. ', [$user]);

            $data = [
                'hours'    => config('settings.timePeriod'),
                'email'    => $user->email,
                'lastDate' => $lastActivation->created_at->format('m/d/Y'),
            ];

            return view('auth.exceeded')->with($data);
        }

        return $this->activeRedirect($user, $currentRoute)
            ->with('status', 'info')
            ->with('message', trans('auth.alreadyActivated'));
    }
}