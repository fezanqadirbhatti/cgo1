<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $user = Auth::user();
        if (Auth::guard($guard)->check() && $user->verified) {
            if ($user->authorizeRoles('admin')) {
                dd('Admin is.');
                return redirect('admin/home');
            }
            if (Auth::user()->authorizeRoles('user')) {
                dd('User is.');
                return redirect('admin/user/dashboard');
            }
        }
        return $next($request);
    }
}
