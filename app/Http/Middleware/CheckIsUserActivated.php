<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckIsUserActivated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (config('settings.activation')) {
            $user = Auth::user();
            if ($user && $user->verified != 1) {
                return redirect()->route('activation-required');
            }
        }
        return $next($request);
    }
}
