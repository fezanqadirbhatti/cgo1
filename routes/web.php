<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{page?}', function ($page='home'){
    //abort(404);
    return view('pages/'.$page);
});

Route::group(['prefix' => 'admin'], function () {
// Admin Dashboard Site
    $this->get('home', 'Admin\AdminHomeController@index')->name('home')->middleware('verified');
    $this->get('user/dashboard', 'Admin\AdminUserDashboardController@index')->name('userDashboard')->middleware('verified');
    $this->get('tracking', 'Admin\AdminTrackController@index')->name('tracking')->middleware('verified');
    $this->get('invoice', 'Admin\AdminInvoiceController@index')->name('invoice')->middleware('verified');
    $this->get('user/profile', 'Admin\AdminUserProfileController@index')->name('userProfile')->middleware('verified');
    $this->post('user/profile/update', 'Admin\AdminUserProfileController@updateProfile')->name('updateProfile')->middleware('verified');

// Authentication Routes...
    $this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
    $this->post('login', 'Auth\LoginController@login');
    $this->post('logout', 'Auth\LoginController@logout')->name('logout');

// Lockscreen middleware
    $name = config('lockscreen.name','lockscreen');
    $url = config('lockscreen.redirect_if_locked', '/lockscreen');
    Route::get('lockscreen', 'LockscreenController@lockscreen');
    Route::middleware('locked')->get($url, '\App\Http\Controller\LockscreenController@showUnlockForm')->name($name);
    Route::middleware('unlock')->post($url, '\App\Http\Controller\LockscreenController@lock');
    Route::middleware('locked')->delete($url, '\App\Http\Controller\LockscreenController@unlock');

// Registration Routes...
    $this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    $this->post('register', 'Auth\RegisterController@register');
    $this->get('verifyemail/{token}', 'Auth\RegisterController@verify')->name('verify.email');
    $this->get('activation-required', 'Auth\ActivateController@activationRequired')->name('activation-required');
    $this->get('activation-resend', 'Auth\ActivateController@resend')->name('resend-activation');

// Password Reset Routes...
    $this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    $this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    $this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    $this->post('password/reset', 'Auth\ResetPasswordController@reset');
});